(clear)


; Open image 

(open $path)


; Draw rectangles on the image

(defn draw-rect 
  (e) 
  (stroke e:rect "rebeccapurple" 10))

(on "mouse-up" draw-rect)


; Write text on the image

(fill 
  (text $xy 50 "is cat") "white" 2)
